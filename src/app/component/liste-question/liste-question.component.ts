import {Component, Input, OnInit} from '@angular/core';
import {QuestionModel} from '../../model/question.model';
import {QuestionsApiService} from '../../service/questions.api.service';

@Component({
  selector: 'app-liste-question',
  templateUrl: './liste-question.component.html',
  styleUrls: ['./liste-question.component.css']
})
export class ListeQuestionComponent implements OnInit {

  listeQuestions: QuestionModel[] = [];

  constructor(
    private questionApi: QuestionsApiService) {
  }

  ngOnInit() {
    this.questionApi.getQuestionsApi().subscribe(questionApi => {
      this.listeQuestions = questionApi;
    } );

  }
}
