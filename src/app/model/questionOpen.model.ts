export class QuestionOpenModel {
  type: string ;
  enonce: string;
  reponse: string;

  constructor(type: string, enonce: string, reponse: string) {
    this.type = type;
    this.enonce = enonce;
    this.reponse = reponse;
  }
}
