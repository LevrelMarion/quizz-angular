import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CreateQuestionComponent } from './component/create-question/create-question.component';
import {HttpClientModule} from '@angular/common/http';
import { AfficherQuestionComponent } from './component/afficher-question/afficher-question.component';
import { ListeQuestionComponent } from './component/liste-question/liste-question.component';
import {QuestionsApiService} from './service/questions.api.service';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  { path: 'afficher', component: ListeQuestionComponent },
  { path: 'creer', component: CreateQuestionComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    CreateQuestionComponent,
    AfficherQuestionComponent,
    ListeQuestionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes),
  ],
  providers: [
    QuestionsApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
