import {Component, Input, OnInit} from '@angular/core';
import {QuestionsApiService} from '../../service/questions.api.service';
import {TypeQuestion} from '../../model/enumTypeQuestion.model';
import {QuestionTrueFalseModel} from '../../model/questionTrueFalse.model';
import {QuestionOpenModel} from '../../model/questionOpen.model';
import {QuestionMultipleModel} from '../../model/questionMultiple.model';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {
  @Input() typeQuestionEnum: TypeQuestion;
  typeQuestion: string;
  reponseSaisie: string;
  questionSaisie: string;
  // suggestionsSaisie: string = [];
  suggestionsSaisie1: string;
  suggestionsSaisie2: string;
  suggestionsSaisie3: string;


  constructor(
    private apiService: QuestionsApiService,
  ) {
  }

  ngOnInit() {
  }

  getTypeQuestion() {
    if (this.typeQuestion === 'OPEN') {
      return 'OPEN';
    } else if (this.typeQuestion === 'MULTIPLE') {
      return 'MULTIPLE';
    } else {
      return 'TRUE_FALSE';
    }
  }

  getSaisieReponse() {
    return this.reponseSaisie;
  }

  getSaisieQuestion() {
  return this.questionSaisie;
  }

  getSaisieSuggestions() {
    return  ;
  }

  postAQuestionOpen() {
    const newQuestionOpen = new QuestionOpenModel
    (this.typeQuestion, this.questionSaisie, this.reponseSaisie);
    this.apiService.postQuestionOpen(newQuestionOpen).subscribe();
    }

    /*
  postAQuestionMultiple() {
    const newQuestionMultiple = new QuestionMultipleModel
    (this.typeQuestion, this.questionSaisie,  , this.reponseSaisie);
    this.apiService.postQuestionMultiple(newQuestionMultiple).subscribe();
  }*/
}
