it('test list questions', () => {
  // verifie que la page s'affiche
  cy.visit('http://localhost:4200/afficher');
  // verifie qu'il y a des div et selectionne la premiere div et si il y a bien du texte dedans
  cy.get('app-liste-question div').eq(1).contains('s\'appelle');
  // verifie que les boutons sont présents
  cy.get('app-liste-question button').should('have.length', 3);
  // verifie qu'il y a un H2
  cy.get('app-liste-question h2');
  // ok l'affichage est vérifié !
});

// maintenant on vérifie l'interraction
it('Test list questions - répondre a une question', () => {
  // instruction a faire avant de lancer le text
  cy.visit('http://localhost:4200/afficher');
  // repondre a la question numero deux et savoir si on peut bien cliquer + verifier la réponse qui est affichée
  cy.get('app-liste-question button').eq(2).click().contains('faux')
  cy.get('app-liste-question div').eq(7).contains('false')
});

it('Test list questions - répondre a une question', () => {
  // instruction a faire avant de lancer le text
  cy.visit('http://localhost:4200/afficher');
  // repondre a la question numero deux et savoir si on peut bien cliquer + verifier la réponse qui est affichée
  cy.get('app-liste-question input').eq(0).type('idéfix')
  // tester si  il y a une methode error (moi je n'en ai pas...)
  // cy.get('app-liste-question .error').contains('pas plus de 10 caractères')
});
