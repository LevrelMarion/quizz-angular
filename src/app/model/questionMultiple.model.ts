export class QuestionMultipleModel {
  type: string;
  enonce: string;
  suggestions: string[];
  reponse: string;


  constructor(type: string, enonce: string, suggestions: string[], reponse: string) {
    this.type = type;
    this.enonce = enonce;
    this.suggestions = suggestions;
    this.reponse = reponse;
  }
}
