export class QuestionModel {
  id: number;
  type: string ;
  enonce: string;
  suggestions: string[];
}
