import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {QuestionModel} from '../model/question.model';
import {ClassResultModel} from '../model/classResult.model';
import {AnswerModel} from '../model/answer.model';
import {TypeQuestion} from '../model/enumTypeQuestion.model';
import {QuestionTrueFalseModel} from '../model/questionTrueFalse.model';
import {QuestionOpenModel} from '../model/questionOpen.model';
import {QuestionMultipleModel} from '../model/questionMultiple.model';

@Injectable()
export class QuestionsApiService {



  constructor(
    private http: HttpClient
  ) {
  }

  getQuestionsApi(): Observable<QuestionModel[]> {
    return this.http.get<QuestionModel[]>('http://localhost:8080/questions');
  }

  getAnswerApi(id: number, bodyAnswer: AnswerModel): Observable<ClassResultModel> {
    return this.http.post<ClassResultModel>(`http://localhost:8080/questions/${id}/answer`, bodyAnswer);
  }

  postQuestionOpen(bodyQuestionOpen: QuestionOpenModel): Observable<QuestionOpenModel> {
    return this.http.post<QuestionOpenModel>(`http://localhost:8080/questions`, bodyQuestionOpen);
  }

  postQuestionMultiple(bodyQuestionMultiple: QuestionMultipleModel): Observable<QuestionMultipleModel> {
    return this.http.post<QuestionMultipleModel>(`http://localhost:8080/questions`, bodyQuestionMultiple);
  }
}
