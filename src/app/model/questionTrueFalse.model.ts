export class QuestionTrueFalseModel {
  type: string ;
  enonce: string;
  reponse: string | boolean;

  constructor(type: string, enonce: string, reponse: string | boolean) {
    this.type = type;
    this.enonce = enonce;
    this.reponse = reponse;
  }
}

