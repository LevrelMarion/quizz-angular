import {Component, Input, OnInit} from '@angular/core';
import {QuestionModel} from '../../model/question.model';
import {QuestionsApiService} from '../../service/questions.api.service';
import {AnswerModel} from '../../model/answer.model';

@Component({
  selector: 'app-afficher-question',
  templateUrl: './afficher-question.component.html',
  styleUrls: ['./afficher-question.component.css']
})
export class AfficherQuestionComponent implements OnInit {
  @Input() question: QuestionModel;
  @Input() reponseSaisie: string;
  isAnswerCorrect: boolean;

  constructor(
    private apiService: QuestionsApiService,
  ) {
  }

  ngOnInit() {

  }

  enregistrer() {
    const answer = new AnswerModel();
    answer.answer = this.reponseSaisie;
    this.apiService.getAnswerApi(this.question.id, answer).subscribe(reponse => {
      if (reponse.result === 'CORRECT') {
        return this.isAnswerCorrect = true;
      } else {
        return this.isAnswerCorrect = false;
      }
    });
  }

  trueFalse(userButton: boolean) {
    const answer = new AnswerModel();
    answer.answer = userButton;
    this.apiService.getAnswerApi(this.question.id, answer).subscribe(reponse => {
      if (reponse.result === 'CORRECT') {
        return this.isAnswerCorrect = true;
      } else {
        return this.isAnswerCorrect = false;
      }
    });
  }

}
